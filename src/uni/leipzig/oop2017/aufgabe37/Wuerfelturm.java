package uni.leipzig.oop2017.aufgabe37;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

/**
 * Created by Nora Brummel & Emil Rode on 01.06.17.
 */
public class Wuerfelturm {
    private Wuerfel[] turm =  new Wuerfel[4];
    HashSet<String[]> kombinationen = new HashSet<>();

    /**
     * getter Methode für die ArrayList an Wuerfeln.
     * @return den Wuerfelturm
     */
    public Wuerfel[] getWuerfel(){ return this.turm; }

    /**
     * neuerWuerfel nimmt einen Würfel entgegen, und setzt ihn an der durch den Parameter Index spezifizierten Stelle ein.
     * @param wuerfel der neue Wuerfel
     * @param index die Stelle, an der der Wuerfel eingesetzt wird.
     */
    public void neuerWuerfel(Wuerfel wuerfel, int index){
        this.turm[index] = wuerfel;
    }

    /**
     * sortRecursive nimmt einen integer wuerfel als Höhe der Rekursion entgegen, sowie einen integer rotationen.
     * Es wird zuerst geprüft, ob die aktuelle Stellung eine Lösung darstellt.
     * Danach wird geprüft, ob die Grenzen von Rotation und Turmhöhe eingehalten werden
     * und in diesem Fall durch rekursive Aufrufe und Rotation durch den Baum der Möglichen Stellungen iteriert.
     *
     * @param wuerfel die Höhe bzw Tiefe des aktuellen Aufrufes
     * @param rotationen Die Anzahl an Rotationen.
     */
    public void sortRecursive(int wuerfel, int rotationen){
        if( isValid() ) {
            String[] seiten = new String[4];
            for (int i = 0; i < 4; i++) {
                StringBuilder sb = new StringBuilder();
                for (int k = 0; k < 4; k++) {
                    sb.append(this.turm[k].getSeite().get(i));
                }
                seiten[i] = sb.toString();
            }
            Arrays.sort(seiten);
            if (!kombinationen.contains(seiten)){
                System.out.println(this.toString());
                kombinationen.add(seiten);
            }
        }
        if (rotationen < 3){
            if (wuerfel < 3){
                turm[wuerfel].rotieren();
                sortRecursive(wuerfel + 1, 0);
                turm[wuerfel].rotieren();
                sortRecursive(wuerfel + 1, 0);
                turm[wuerfel].rotieren();
                sortRecursive(wuerfel + 1, 0);
                turm[wuerfel].rotieren();
            } else {
                turm[wuerfel].rotieren();
                sortRecursive(wuerfel, rotationen + 1);
            }
        }
    }

    /**
     * isValid gibt true zurück, für den Fall, dass alle Seitenwände niemals
     * zweimal die gleiche Farbe enthalten.
     * @return boolean true für aktzeptiere Stellungen. boolean false für nicht aktzeptierte.
     */
    public boolean isValid(){
        HashSet<String> farben = new HashSet<>();
        for (int i = 0; i < 4; i++){
            for (int k = 0; k < 4; k++){
                if( !farben.contains( turm[k].getSeite().get(i) ) ){
                    farben.add( turm[k].getSeite().get(i) );
                }
                else { return false; }                                  //returns false if the colour already appeared
            }
            farben.clear();
        }
        return true;                                                    //returns true if all the columns of colours are checked
    }

    /**
     * toString gibt einen Wuerfelturm als String zurück.
     * Dabei wird ein Stringbuilder benutzt.
     * @return Stringrepräsentation des Turmes
     */
    public String toString(){
        StringBuilder result = new StringBuilder();
        result.append("###########").append("\n");
        for (int i = 0; i < 4; i++){
            result.append("\n").append(turm[i].toString()).append("\n");
        }
        result.append("\n");
        return result.toString();
    }
}
