package uni.leipzig.oop2017.aufgabe37;

import com.sun.tools.classfile.Opcode;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by Nora Brummel and Emil Rode on 01.06.17.
 */
public class Wuerfel {
    /**
     * private static HashSet farbe : HashSet, das die möglichen Farben enthält
     * private ArrayList seite : ArrayList, das die vier Seiten eines Würfels enthält
     * private String top : obere Seite des Würfels
     * private String bottom : untere Seite des Würfels
     */

    private static HashSet<String> farbe = new HashSet(){{
        add("rot");
        add("blau");
        add("grün");
        add("gelb");
    }};
    private ArrayList<String> seite = new ArrayList();
    private String top;
    private String bottom;

    /**
     * public Wuerfel() : Konstruktor der Top, Bottom und Seiten des Würfels enthält
     * @param top
     * @param seite
     * @param bottom
     */
    public Wuerfel( String top, ArrayList<String> seite, String bottom ) {
        this.top = top;
        this.seite = seite;
        this.bottom = bottom;
    }

    /**
     * public String getTop() : gibt obere Seite des Würfels zurück - Getter für Top
     * @return top
     */
    public String getTop() {
        return top;
    }

    /**
     * public String getBottom() : gibt untere Seite des Würfels zurück - Getter für Bottom
     * @return bottom
     */
    public String getBottom() {
        return bottom;
    }

    /**
     * public ArrayList getSeite() : gibt Seite des Würfels zurück - Getter für Seite
     * @return seite
     */
    public ArrayList<String> getSeite() {
        return seite;
    }

    /**
     * public HashSet getFarbe() : gibt Farbe der Seite zurück - Getter für Farbe
     * @return farbe
     */
    public static HashSet<String> getFarbe() {
        return farbe;
    }

    /**
     * public String toString() : gibt Würfelfarben und -seiten in Stringformat zurück
     * @return wuerfelString.toString()
     */
    public String toString() {
        StringBuilder wuerfelString = new StringBuilder();
        wuerfelString.append(top).append("\n").append(seite).append("\n").append(bottom);
        return wuerfelString.toString();
    }

    /**
     * public void rotieren() : rotiert die Seiten des Wuerfels, indem die erste Seite an der Stelle [0] hinten
     * hinzugefügt und am Anfang der ArrayList gelöscht wird
     */
    public void rotieren() {
        seite.add(seite.get(0));
        seite.remove(seite.get(0));
    }
}
