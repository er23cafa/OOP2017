package uni.leipzig.oop2017.aufgabe37;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by Nora Brummel & Emil Rode on 01.06.17.
 */
public class Main {

    public static void main(String args[]) {
        /**
         * Scanner nutzerEingabe : Scanner für Farbeingaben des Nutzers
         */
        Scanner nutzerEingabe = new Scanner(System.in);
        Wuerfelturm turm = new Wuerfelturm();
        for (int i = 0; i < 4; i++) {
            UserInputLoop:
            //UserInputLoop : Loop für Nutzereingaben
            while (true) {
                try {
                    System.out.println("Bitte geben Sie vier Würfel mit der jeweils gewünschten Farbkombination ein. \n" +
                            "Zur Auswahl stehen die folgenden vier Farben: \n" + "*rot\n" + "*blau\n" + "*grün\n" + "*gelb\n" +
                            "\n" + "Bitte trennen Sie die Farben mit jeweils einem Leerzeichen. \n" + "Ihre Eingabe:\n");
                    System.out.println("Top:");
                    String eingabeTop = nutzerEingabe.nextLine();
                    System.out.println("Seiten:");
                    String eingabeSeiten = nutzerEingabe.nextLine();
                    String[] eingabeSeitenSplit = eingabeSeiten.split(" ");
                    System.out.println("Bottom:");
                    String eingabeBottom = nutzerEingabe.nextLine();

                    String[] eingabeFarben = new String[6];
                    eingabeFarben[0] = eingabeTop;
                    eingabeFarben[1] = eingabeSeitenSplit[0];
                    eingabeFarben[2] = eingabeSeitenSplit[1];
                    eingabeFarben[3] = eingabeSeitenSplit[2];
                    eingabeFarben[4] = eingabeSeitenSplit[3];
                    eingabeFarben[5] = eingabeBottom;


                    if (!(Wuerfel.getFarbe().contains(eingabeFarben[0]))) {
                        System.out.println("Sie haben einen Fehler bei 'Top' gemacht. Bitte überprüfen Sie Ihre Eingabe bezüglich Rechtschreibung und Leerzeichen.\n");
                        continue UserInputLoop;
                    }
                    // wird ausgegeben, wenn ein Fehler bei Top-Initialisierung vorliegt
                    if (!(Wuerfel.getFarbe().contains(eingabeFarben[1]) && Wuerfel.getFarbe().contains(eingabeFarben[2]) &&
                            Wuerfel.getFarbe().contains(eingabeFarben[3]) && Wuerfel.getFarbe().contains(eingabeFarben[4]))) {
                        System.out.println("Sie haben einen Fehler bei 'Seiten' gemacht. Bitte überprüfen Sie Ihre Eingabe bezüglich Rechtschreibung und Leerzeichen.\n");
                        continue UserInputLoop;
                    }
                    // wird ausgegeben, wenn ein Fehler bei der Seiten-Initialisierung vorliegt
                    if (!(Wuerfel.getFarbe().contains(eingabeFarben[5]))) {
                        System.out.println("Sie haben einen Fehler bei 'Bottom' gemacht. Bitte überprüfen Sie Ihre Eingabe bezüglich Rechtschreibung und Leerzeichen.\n");
                        continue UserInputLoop;
                    }
                    turm.neuerWuerfel(new Wuerfel(eingabeTop, new ArrayList<String>(Arrays.asList(eingabeSeitenSplit[0],
                            eingabeSeitenSplit[1], eingabeSeitenSplit[2], eingabeSeitenSplit[3])), eingabeBottom), i);
                    break UserInputLoop;
                    // wird ausgegeben, wenn ein Fehler bei der Bottom-Initialisierung vorliegt
                } catch (ArrayIndexOutOfBoundsException e) {
                    System.out.println("Sie haben einen Fehler gemacht. Bitte überprüfen Sie Ihre Eingabe bezüglich Rechtschreibung und Leerzeichen.\n +" +
                            "Bitte beachten Sie, dass Sie bei Top und Bottom jeweils eine Farbe und bei Seiten genau vier Farben angeben müssen.\n");
                    continue UserInputLoop;
                }
                // fängt ArrayIndexOutOfBoundExceptions
            }
        }
        turm.sortRecursive(0, 0);
    }
    }

