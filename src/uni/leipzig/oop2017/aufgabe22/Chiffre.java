package uni.leipzig.oop2017.aufgabe22;

import java.util.*;

/**
 * Created by user on 04/05/2017.
 */
public class Chiffre {
    /**
     * private static char[] klartextAlphabet : char Array, das Kleinbuchstaben des Alphabets nach vorgegebener Reihenfolge enthält
     * private static HashSet<Character> asciiChars : HashSet, das Kleinbuchstaben des Alphabets nach vorgegebener Reihenfolge enthält
     * private static char[] geheimtextAlphabet : char Array der Länge von klartextAlphabet
     */
    private static char[] klartextAlphabet = new char[] {'z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
                                                  'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y'};
    private static HashSet<Character> asciiChars= new HashSet<Character>(
            Arrays.asList('z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
                    'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y'));
    private static char[] geheimtextAlphabet = new char[klartextAlphabet.length];

    /**
     * private static int getIndex(char c) : Hilfsfunktion, die Zugriff auf Index ermöglicht
     */
    public static int getIndex(char c) {
        for (int k = 0; k < klartextAlphabet.length; k++) {
            if (c == klartextAlphabet[k]) {
                return k;
            }
        }
        return -1;
    }

    /**
     *private static ArrayList frequentChar(String[] text) : Hilfsfunktion, um herauszufinden wie oft ein Charakter im Text vorkommt
     *@param text
     * @return list
     */
    public static ArrayList<HashMap.Entry<Character, Integer>> frequentChar(String[] text){
        HashMap<Character, Integer> map = new HashMap<Character, Integer>();
        for(int i = 0; i < text.length; i++) {
            String line = text[i];
            for (int k = 0; k < text[i].length(); k++) {
                char character = Character.toLowerCase(text[i].charAt(k));
                if (asciiChars.contains(character)) {
                    Integer value = map.get(character);
                    if (value != null) {
                        map.put(character, value + 1);
                    } else {
                        map.put(character, 1);
                    }
                }
            }
        }
                ArrayList<HashMap.Entry<Character, Integer>> list =
                        new ArrayList<HashMap.Entry<Character, Integer>>( map.entrySet() );
                Collections.sort( list, new Comparator<HashMap.Entry<Character, Integer>>() {
                    public int compare( HashMap.Entry<Character, Integer> entry1, HashMap.Entry<Character, Integer> entry2 ) {
                        return (entry1.getValue()).compareTo( entry2.getValue() );
                    }
                } );
                return list;
    }

    /**
     * public static String[] chiffrieren(int schluessel String[] klarText) : Funktion verschlüsselt Text und gibt diesen verschlüsselt zurück
     * @param schluessel
     * @param klarText
     * @return result
     */
    public static String[] chiffrieren(int schluessel, String[] klarText) {
        StringBuilder line;
        String[] result = new String[klarText.length];
        for (int i = 0; i < klarText.length; i++){
            line = new StringBuilder();
            for (int k = 0; k < klarText[i].length(); k++){
                char buchstabe = Character.toLowerCase(klarText[i].charAt(k));
                if (asciiChars.contains( buchstabe ) ){
                    line.append(klartextAlphabet[(getIndex(buchstabe) * schluessel) % 26]);
                    //handelt es sich um einen 'normalen' Charakter, wird er nach oben stehender Formel verschlüsselt
                }
                else{
                    line.append(buchstabe);
                    //handelt es sich um einen anderen Charakter, wird er einfach dem Text hinzugefügt
                }
            }
            result[i] = line.toString();
        }
        return result;
    }

    /**
     * public static String[] chiffrieren(int schluessel, String[] geheimtext) : Funktion entschlüsselt Text und gibt diesen entschlüsselt zurück
     * @param schluessel
     * @param geheimtext
     * @return result
     */
    public static String[] dechiffrieren(int schluessel, String[] geheimtext){
        StringBuilder line;
        String[] result = new String[geheimtext.length];
        for (int i = 0; i < geheimtext.length; i++){
            line = new StringBuilder();
            for (int k = 0; k < geheimtext[i].length(); k++){
                char ch = geheimtext[i].charAt(k);
                if (asciiChars.contains(Character.toLowerCase( geheimtext[i].charAt(k) ) ) ){
                    int n = 0;
                    while(((n * 26) + getIndex(ch)) % schluessel != 0 ) {
                        n++;
                    }
                    line.append(klartextAlphabet[((n * 26) + getIndex(ch) ) / schluessel]);
                    //handelt es sich um einen normalen Charakter, wird dieser dechiffriert nach obenstehender Formel
                }
                else{
                    line.append(ch);
                    //handelt es sich um einen anderen Charakter, wird dieser einfach übernommen
                }
            }
            result[i] = line.toString();
        }
        return result;
    }
}
