package uni.leipzig.oop2017.aufgabe22;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

import static java.nio.file.Files.newBufferedWriter;

/**
 * Created by user on 04/05/2017.
 */
public class IO {
    /**Der Scanner 'scanner' wird für die Nutzereingabe gebraucht.*/
    private static Scanner scanner = new Scanner(System.in);

    /**
     * Die Methode lesen(String file) nimmt einen Dateipfad entgegen und ließt dann aus der in UTF-16 codierten Datei Text aus.
     * Das lesen erfolgt Zeile für Zeile per BufferedReader und die einzelnen Zeilen werden in ein Array von Strings geschrieben,
     * welches bei Erfolg zurückgegebn wird.
     * @param file Der Dateipfad des Files
     * @return den eingelesenen Text als String-Array.
     */
    public static String[] lesen(String file) {
        Charset charset = Charset.forName("UTF-16");
        ArrayList<String> lines = new ArrayList<>();

        try (BufferedReader reader = Files.newBufferedReader(Paths.get(file), charset)) {
            String line;
            while ((line = reader.readLine()) != null) {
                lines.add(line);
            }
            reader.close();
        } catch (IOException x) {
            System.err.format("IOException: %s%n", x);
        }
        return lines.toArray(new String[0]);
    }

    /**
     * Die Methode schreiben(String[] lines, String file) nimmt ein Array aus Strings und einen Dateipfad entgegen.
     * Die Strings des Array werden Zeile für Zeile per BufferedWriter in die Datei geschrieben.
     * Bei Erfolg wird true zurückgegeben.
     * @param lines Das Array der Zeilen die geschrieben werden.
     * @param file Der Dateipfad der zu schreibenden Datei.
     * @return boolean true für Erfolg und boolean false für Misserfolg.
     */
    public static boolean schreiben(String[] lines, String file){
        Charset charset = Charset.forName("UTF-16");
        try (BufferedWriter writer = newBufferedWriter(Paths.get(file), charset)) {
            for(int i = 0; i<lines.length; i++) {
                writer.write(lines[i], 0, lines[i].length());
                writer.newLine();
            }
            writer.close();
            return true;
        } catch (IOException e) {
            System.out.print(e.getMessage());
            return false;
        }
    }

    /**
     * Die Methode getNutzerEingabe() benutzt den Scanner um eine Nutzereingabe über das Terminal einzulesen und
     * sie dann als String zurückzugeben.
     * @return den eingelesenen String.
     */
    public static String getNutzerEingabe(){
        return scanner.nextLine();
    }
}
