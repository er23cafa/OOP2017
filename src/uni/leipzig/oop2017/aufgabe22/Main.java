package uni.leipzig.oop2017.aufgabe22;


import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by user on 04/05/2017.
 */

public class Main {

    public static void main(String args[]) {
        String path = "/Users/user/IdeaProjects/OOP2017/uni/";
        IO.schreiben(IO.lesen(path + "Gedicht.txt"), path + "Gedicht_write.txt");

        for (int i = 0; i < IO.lesen(path + "Gedicht.txt").length; i++) {
            System.out.println(IO.lesen(path + "Gedicht.txt")[i]);
            //Gedicht wird ausgegeben
        }

        schluesselLoop1:
        while (true) {
            int schluessel =0;
            try{
                System.out.println("Bitte geben Sie einen Schlüssel ein, nach dem der Text chiffriert werden soll\n" +
                        "(Mögliche Schlüssel: 1, 3, 5, 7, 9, 11, 15, 19, 21, 23, 25): ");
                schluessel = Integer.parseInt(IO.getNutzerEingabe());
                if (schluessel != 1 && schluessel != 3 && schluessel != 5 && schluessel != 7 && schluessel != 9 && schluessel != 11 && schluessel != 15 && schluessel != 19 && schluessel != 21 && schluessel != 23 && schluessel != 25) {
                    System.out.println("Der Schlüssel ist nicht möglich");
                    System.out.println();
                    continue schluesselLoop1;
                }
                //schluessel wird eingelesen
                String[] verschluesselt = Chiffre.chiffrieren(schluessel, IO.lesen(path + "Gedicht.txt"));
                IO.schreiben(verschluesselt, path + "Gedicht_verschluesselt.txt");
                //datei mit verschlüssltem Text
            }catch(NumberFormatException e){
            System.out.println("Der Schlüssel ist nicht möglich");
            continue schluesselLoop1;
           }

            switchLoop:
            while (true) {
                try {
                    System.out.println("\nKennen Sie den Schlüssel zum entschlüsseln des Gedichtes? (JA/NEIN)");
                    String eingabe = IO.getNutzerEingabe();
                    //switch für Nutzereingaben
                    switch (eingabe.toUpperCase()) {
                        case "JA":
                            System.out.print("Bitte geben Sie den Schlüssel zum entziffern an: ");
                            schluessel = Integer.parseInt(IO.getNutzerEingabe());
                            schluesselLoop2:
                            while (true) {
                                if (schluessel != 1 && schluessel != 3 && schluessel != 5 && schluessel != 7 && schluessel != 9 && schluessel != 11 && schluessel != 15 && schluessel != 19 && schluessel != 21 && schluessel != 23 && schluessel != 25) {
                                    System.out.println("Der Schlüssel ist nicht möglich");
                                    System.out.println();
                                    continue schluesselLoop2;

                                } else {
                                    String[] verschluesselt = Chiffre.chiffrieren(schluessel, IO.lesen(path + "Gedicht.txt"));
                                    String[] entschluesselt = Chiffre.dechiffrieren(schluessel, verschluesselt);
                                    IO.schreiben(entschluesselt, path + "Gedicht_entschluesselt.txt");
                                    break schluesselLoop2;
                                }

                            }
                            break;
                        case "NEIN":
                            int x = 0;
                            String[] verschluesselt = Chiffre.chiffrieren(schluessel, IO.lesen(path + "Gedicht.txt"));
                            ArrayList<HashMap.Entry<Character, Integer>> list = Chiffre.frequentChar(verschluesselt);
                            String[] entschluesselt;
                            schluesselEvaluationsLoop:
                            while (true) {
                                Chiffre.getIndex(list.get(list.size() - (1 + x)).getKey());
                                    int n = 0;
                                    while(((n * 26) + Chiffre.getIndex(list.get(list.size() - (1 + x)).getKey())) % Chiffre.getIndex('e') != 0 ) {
                                        n++;
                                    }
                                schluessel = ((n * 26) + Chiffre.getIndex(list.get(list.size() - (1 + x)).getKey())) / Chiffre.getIndex('e');
                                entschluesselt = Chiffre.dechiffrieren(schluessel, verschluesselt);
                                for (int i = 0; i < entschluesselt.length; i++) {
                                    System.out.println(entschluesselt[i]);
                                }
                                System.out.println("\n Macht das Sinn? (Ja/Nein)");
                                sinnLoop:
                                while (true) {
                                    switch (IO.getNutzerEingabe().toUpperCase()) {
                                        case "JA":
                                            break schluesselEvaluationsLoop;
                                        case "NEIN":
                                            continue schluesselEvaluationsLoop;
                                        default:
                                            System.out.println("Bitte Ja oder Nein eingeben!");
                                            continue sinnLoop;
                                    }
                                }
                            }
                            IO.schreiben(entschluesselt, path + "Gedicht_entschluesselt.txt");
                            break;
                        //code norbert
                        case "CODE NORBERT":
                            for (int i = 0; i < IO.lesen(path + "Gedicht.txt").length; i++) {
                                String text = IO.lesen(path + "Gedicht.txt").toString();
                                System.out.println(text.replaceAll("[a-z]", "Norbert was here "));
                            }
                            break;
                        default:
                            System.out.println("Bitte JA oder NEIN eingeben.");
                            continue switchLoop;
                    }
                }catch (NumberFormatException x) {
                    System.out.println(x.getMessage());
                    continue switchLoop;
                }
                System.exit(0);
            }
        }
    }
}
