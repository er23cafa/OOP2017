package uni.leipzig.oop2017.aufgabe06;
import java.util.Scanner;

/**
 * Created by Nora Brummel und Emil Rode on 10/04/2017.
 * @version 1.0
 */
public class Spiel {
    /**
     * Spieler[] spieler: statisches Feld, in dem Spielerdaten gespeichert sind
     * String spielregeln: Spielregeln des Spiels
     * int spielrunde: Anzahl der Spielrunden
     * Steinhaufen steinhaufen: Steinhaufen, der für das Spiel benutzt wird
     * int aktuellerSpieler: Spieler, der an der Reihe ist
     * Scanner eingabeScanner: Scanner, der die Eingaben des Spielers regelt
     */
    private static Spieler[] spieler;
    private final String spielregeln =
            "Das Nimmspiel ist ein Strategiespiel für zwei Personen. " +
                    "\nEin Spieler und der Computer als zweiter Spieler nehmen Steine von einem Steinhaufen, bis kein Stein mehr vorhanden ist." +
                    "\nDie Anzahl der Steine im Steinhaufen (20-30) und der Startspieler werden zufällig festgelegt. " +
            "\nDie Spieler nehmen im Wechsel 1 bis 3 Steine vom Steinhaufen. " +
            "\nVerloren hat der Spieler, der den letzten Stein nehmen muss." +
            "\nViel Spaß!";
    private int spielrunde;
    private Steinhaufen steinhaufen;
    private int aktuellerSpieler;
    private Scanner eingabeScanner = new Scanner(System.in);

    /**
     * Kontruktor Spiel(): konstruiert ein Spiel, das die aktuellen Spieldaten (Spieler, Spielrunde, Anzahl der Steine im Steinhaufen,
       aktueller Spieler) enthält
     */
    public Spiel(){
        System.out.println(spielregeln);
        System.out.println();
        this.spieler = new Spieler[2];
        System.out.print("Bitte Name eingeben: ");
        String username = eingabeScanner.nextLine();
        /*
            Leere Namenseingaben werden abgefangen under Spieler per Usereingabe benannt
         */
        while(username.equals("")){
            System.out.print("Du Dummbaddel musst einen Namen wählen!\nVersuchs nochmal: ");
            username = eingabeScanner.nextLine();
        }
        this.spieler[0] = new Spieler(username); //Der menschliche Spieler gibt seinen Namen an
        System.out.println("\nSei gegrüßt " + spieler[0].getName() + "!!!");

        /*
            Es gibt einen easymode wenn man Norbert heisst
         */
        if(spieler[0].getName().toUpperCase().equals("NORBERT")){
            spieler[1] = new Computer("Easy Computer");
            System.out.println("Easymode enabled!!!");  //Norbert spielt nur gegen Easy Computer
        } else { spieler[1] = new Computer("Computer"); }

        this.spielrunde = 1;
        this.steinhaufen = new Steinhaufen(new Wuerfel(20, 30).wuerfeln()); //Zu Spielbeginn zufällig viele Steine zwischen 20 und 30
        this.aktuellerSpieler = new Wuerfel(0, 1).wuerfeln();   //Der Startspieler wird gewürfelt

        /*
            Spiel-Schleife: Schleife des aktuellen Spiels, die läuft, bis keine Steine mehr im Steinhaufen vorhanden sind
        */
        while(steinhaufen.getSteine() != 0){
            spieler[aktuellerSpieler].takeSteine(this);
            aktuellerSpieler = 1 - aktuellerSpieler;
        }

        System.out.println("\n" + spieler[aktuellerSpieler].getName() + " hat gewonnen.");
        System.out.println("Spielrunde:                     " + spielrunde);
        System.out.println("Vom Computer genommene Steine:  " + ((Computer) spieler[1]).getAnzahlSteine());
        System.out.println("\nSpiel zuende. Möchtest du eine neue Runde beginnen? (JA/NEIN)");
        while( true ){
            switch(eingabeScanner.nextLine().toUpperCase()){
                case "JA":      new Spiel();
                    break;
                case "NEIN":    System.out.println("\nBis zum nächsten mal " + spieler[0].getName() + "! <3");
                                System.exit(0);
                    break;
                default:        System.out.println("Was kannst du eigentlich? Es hieß doch 'JA oder NEIN'");
                                System.out.println("Versuchs nochmal:");

            }
        }
        /*
            while- Schleife: regelt über Spielereingabe, ob ein neues Spiel gestartet wird oder nicht
            case "JA": neues Spiel wird gestartet
            case "NEIN": kein neues Spiel wird gestartet
            default: wird ausgegeben, wenn Spieler etwas anderes als JA oder NEIN eingibt
         */
    }

    /**
     * Getter getSteinhaufen(): ermöglicht den Zugriff auf Steinhaufen über andere Klassen
     * @return
     */
    public Steinhaufen getSteinhaufen(){
        return this.steinhaufen;
    }

    /**
     * Getter getSpielregeln(): gibt Spielregeln wieder
     * @return
     */
    public String getSpielregeln() {
        return spielregeln;
    }

    /**
     * Getter getSpielrunde(): gibt Spielrunde wieder
     * @return
     */
    public int getSpielrunde() {
        return spielrunde;
    }

    /**
     * Getter getEingabeScanner(): gibt Eingabe des Spielers wieder
     * @return
     */
    public Scanner getEingabeScanner() {
        return eingabeScanner;
    }

    public void setSpielrunde(int spielrunde) {
        this.spielrunde = spielrunde;
    }
}
