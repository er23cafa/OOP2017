package uni.leipzig.oop2017.aufgabe06;

/**
Wuerfel gedownloaded von: http://www.informatik.uni-leipzig.de/bsv/homepage/de/teaching/praktikum-objektorientierte-programmierung-ss-2017
 */
public class Wuerfel {

    private int min;
    private int max;
    private int augen;


    public Wuerfel(int var1, int var2) {
        if(var2 < var1) {
            this.min = var2;
            this.max = var1;
        } else {
            this.min = var1;
            this.max = var2;
        }

        this.wuerfeln();
    }

    public Wuerfel() {
        this(1, 6);
    }


    public String toString() {
        return "Wuerfel " + this.min + " .. " + this.max + " Augen: " + this.augen;
    }


    public int getAugen() {
        return this.augen;
    }

    public int wuerfeln() {
        this.augen = (int)(Math.random() * (double)(this.max - this.min + 1)) + this.min;
        return this.augen;
    }

    public static void main(String[] var0) {
        byte var1 = 1;
        byte var2 = 9;
        Wuerfel var3 = new Wuerfel(var1, var2);
        byte var4 = 10;
        System.out.println();
        System.out.println(var3);
        System.out.println();
        System.out.println(var4 + " x wuerfeln");
        System.out.println();

        for(int var6 = 0; var6 < var4; ++var6) {
            var3.wuerfeln();
            System.out.print(var6 + ". Wurf: ");
            System.out.println(var3.getAugen());
        }

        System.out.println();
        System.out.println("Programm beendet");
    }
}

