package uni.leipzig.oop2017.aufgabe06;

/**
 * Created by Nora Brummel und Emil Rode on 10/04/2017.
 * @version 1.0
 */
public class Spieler {
    /**
     * String name: Name des Spielers, wird von Spieler gesetzt (im Fall des Computers immer "Computer")
     */
    private String name;

    /**
     * Konstruktor Spieler(): konstruiert Spieler mit dem eingegebenen Name
     * @param name
     */

    public Spieler(String name){
        this.name = name;
    }

    /**
     * Getter getName(): gibt Name des Spielers wieder
     * @return
     */
    public String getName(){
        return this.name;
    }

    /**
     * takeSteine(): Spieler gibt an, wieviele Steine er nehmen möchte
     * @param spiel
     */
    public void takeSteine(Spiel spiel){
        System.out.println("Steine im Steinhaufen:          " + spiel.getSteinhaufen().getSteine());
        System.out.println("\nBitte gib ein, wieviele Steine du nehmen willst: ");
        boolean amZug = true;
        while (amZug) {
            try {
                int eingabe = Integer.valueOf(spiel.getEingabeScanner().nextLine());
                if (eingabe >= 1 && eingabe <= 3) {

                    if (spiel.getSteinhaufen().getSteine() < eingabe) {
                        System.out.println("Dieser Zug ist nicht möglich. Bitte nimm eine andere Anzahl an Steinen.");
                    }
                /*
                *   wird ausgegeben, wenn nicht genügend Steine im Steinhaufen für gewählte Anzahl an Steinen mehr vorhanden sind
                */
                    else {
                        spiel.getSteinhaufen().setSteine(spiel.getSteinhaufen().getSteine() - eingabe);
                        amZug = false;
                    }
                /*
                *   sind noch genügend Steine im Steinhaufen, wird die gewählte Anzahl aus dem Steinhaufen entfernt
                */
                } else {
                    System.out.println("Dieser Zug ist nicht möglich. Bitte gib eine gültige Anzahl an Steinen ein, die du nehmen möchtest.");
                }
                /*
                *   wird ausgegeben, wenn der Spieler etwas anderes als eine Zahl zwischen 1 und 3 angibt
                */
            }
            catch (NumberFormatException e){ System.out.println("Erlaubt sind nur Zahlen von 1 - 3"); }
        }
        spiel.setSpielrunde(spiel.getSpielrunde() + 1);
    }
}
