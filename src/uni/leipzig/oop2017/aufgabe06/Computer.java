package uni.leipzig.oop2017.aufgabe06;

/**
 * Created by Nora Brummel und Emil Rode on 10/04/2017.
 * @version 1.0
 */
public class Computer extends Spieler {
    /**
     * int anzahlSteine: die vom Computer genommene Anzahl an Steinen
     */
    private int anzahlSteine;

    /*
        Der Easy Computer wuerfelt am Anfang, statt optimal zu spielen
     */
    private Wuerfel random;

    /**
     * Konstruktor Computer(): konstruiert Spieler mit Namen "Computer" der zu Anfang des Spiels noch keine Steine gezogen hat
     */
    public Computer(String name){
        super(name);
        this.anzahlSteine = 0;
        this.random = new Wuerfel(1, 3);
    }
    /**
     * takeSteine(): überschreibt Methode takeSteine() der Klasse Spieler; regelt, wieviele Steine der Computer nehmen muss,
       um strategisch optimal zu spielen
     * @param spiel
     */
    @Override public void takeSteine(Spiel spiel){
        Steinhaufen steinhaufen = spiel.getSteinhaufen();   //Der Steinhaufen wird hier in "steinhaufen" abgelegt
        /*
            Falls es sich um einen "Easy Computer" handelt, würfelt dieser bei einem vollerem Steinhaufen
         */
        if(steinhaufen.getSteine() >= 7 && this.getName().equals("Easy Computer")) {
            takeSteine(steinhaufen, random.wuerfeln());
        }
        else{
            /*
                von den im Steinhaufen vorhandenen Steinen wird einer abgezogen und die Übrigbleibenden durch vier geteilt.
                je nachdem, wieviel der Rest der Division beträgt, wird eine Anzahl an Steinen genommen
            */
            int i = (steinhaufen.getSteine() - 1) % 4;
            switch (i) {
                case 0:
                    takeSteine(steinhaufen, 1); //ist der Rest der Division = 0, nimmt der Computer einen Stein
                    break;

                case 1:
                    takeSteine(steinhaufen, 1); //ist der Rest der Division = 1, nimmt der Computer einen Stein
                    break;

                case 2:
                    takeSteine(steinhaufen, 2); //ist der Rest der Division = 2, nimmt der Computer zwei Steine
                    break;

                case 3:
                    takeSteine(steinhaufen, 3); //ist der Rest der Division = 3, nimmt der Computer drei Steine
                    break;
            }
        }
    }
    
    private void takeSteine(Steinhaufen steinhaufen, int anzahl){
        steinhaufen.setSteine(steinhaufen.getSteine() - anzahl);
        anzahlSteine = anzahlSteine + anzahl;
        System.out.println("\nDer Computer hat " + anzahl + " Steine genommen");
    }

    /**
     * Getter getAnzahlSteine(): gibt die vom Computer genommenen Steine wieder
     * @return
     */
    public int getAnzahlSteine(){
        return this.anzahlSteine;
    }
}
