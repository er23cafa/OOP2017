package uni.leipzig.oop2017.aufgabe06;

/**
 * Created by Nora Brummel und Emil Rode on 10/04/2017.
 * @version 1.0
 */
public class Steinhaufen {
    /**
     * int steine: Anzahl der Steine im Steinhaufen
     */
    private int steine;

    /**
     * Konstruktor Steinhaufen(int): konstruiert einen Steinhaufen mit der übergebenen Anzahl an Steinen
     * @param steine
     */
    public Steinhaufen(int steine){
        this.steine = steine;
    }

    /**
     * Getter getSteine(): gibt Anzahl der (verbliebenen) Steine im Steinhaufen wieder
     * @return
     */
    public int getSteine() {
        return steine;
    }

    /**
     * Setter setSteine(): setzt die Anzahl der Steine im Steinhaufen auf die übergebene Anzahl
     * @param steine
     */
    public void setSteine(int steine) {
        this.steine = steine;
    }
}
