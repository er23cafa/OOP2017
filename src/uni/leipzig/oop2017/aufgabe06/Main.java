package uni.leipzig.oop2017.aufgabe06;

/**
 * Entry point des Programmes
 *
 * @author Emil C. Rode / Nora Pauline Brummel
 * @version 1.0
 */
public class Main {

    public static void main(String[] args) {
        Spiel meinSpiel = new Spiel();
    }
}
