package uni.leipzig.oop2017.aufgabe13;

/**
 * Created by Emil Rode und Nora Brummel on 20.04.17.
 */
public class Schmetterling {

    /**
     * String musterung: variierende Musterung des Schmetterlings
     * String fluegelfarbe: variierende Flügelfarbe des Schmetterlings
     * String fuehlerform: variierende Fühlerform des Schmetterlings
     */
    private String musterung;
    private String fluegelfarbe;
    private String fuehlerform;

    /**
     * Konstruktor Schmetterling(): konstruiert Schmetterling mit gewünschten Merkmalen
     * @param musterung
     * @param fluegelfarbe
     * @param fuehlerform
     */
    public Schmetterling(String musterung, String fluegelfarbe, String fuehlerform){
        this.musterung = musterung;
        this.fluegelfarbe = fluegelfarbe;
        this.fuehlerform = fuehlerform;
    }

    /**
     * Getter getMerkmale(): gibt Merkmale des Schmetterlings als String[] wieder
     * @return
     */
    public String[] getMerkmale() {

        return new String[]{musterung, fluegelfarbe, fuehlerform};
    }
}