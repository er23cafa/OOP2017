package uni.leipzig.oop2017.aufgabe13;

/**
 * Created by user on 24/04/2017.
 */
public class Brut {
    /**
     * Schmetterling[] schmetterlinge: Die Gesamtheit der Schmetterlinge in der Brut.
     */
    private Schmetterling[] schmetterlinge;
    /**
     * String[] elternMerkmale: Die Merkmale der Eltern.
     */
    private String[] elternMerkmale;
    /**
     * String[] musterOptionen: Musteroption für späteren Vergleich.
     */
    private static final String[] musterOptionen = new String[]{"uni", "gepunktet", "gestreift"};
    /**
     * String[] farbOptionen: Farboptionen für späteren Vergleich.
     */
    private static final String[] farbOptionen = new String[]{"rot", "gelb", "grün", "blau"};
    /**
     * String[] fuehlerOptionen: Fühleroptionen für späteren Vergleich.
     */
    private static final String[] fuehlerOptionen = new String[]{"gerade", "gekrümmt"};

    /**
     * Konstruktor der Brut. Die Groesse wird ueber randomZahl() festgelegt und dann ein Array dieser Groesse erstellt.
     * Das Array wird mit Schmetterlingen gefuellt, die eine zufaellige Kombination aus Merkmalen der Eltern aufweisen.
     * @param elternMerkmale String[]
     */
    public Brut(String[] elternMerkmale){
        int anzahl = randomZahl();                      //Die Größe der Brut wird festgelegt
        schmetterlinge = new Schmetterling[anzahl];     //Ein neues Array der zufälligen Größe anzahl wird initialisiert
        this.elternMerkmale = elternMerkmale;           //Die Merkmale der Eltern werden für spätere Verwendung gespeichert.

        for( int i = 0; i < anzahl; i++){               //Die for-Schleife erschafft anzahl viele Schmetterlinge aus den Merkmalen der Eltern
            schmetterlinge[i] = new Schmetterling(randomMerkmal(elternMerkmale[0], elternMerkmale[3]),
                    randomMerkmal(elternMerkmale[1], elternMerkmale[4]), randomMerkmal(elternMerkmale[2], elternMerkmale[5]));
        }
    }

    /**
     * Auswerten wertet die Schmetterlingsbrut nach einzelnen Merkmalen prozentual aus.
     * Die Prozentsaetze für jedes Merkmal jedes Schmetterlings der Brut wird im Terminal ausgegeben.
     */
    public void auswerten(){
        /*
            Der Zähler wird als Array initialisiert, das die Anzahl der Schmetterlinge,
            die ein bestimmtes Merkmal aufweisen für jedes Merkmal einzeln speichert.
         */
        int[] zaehler = new int[3];

        for(int k = 0; k < musterOptionen.length; k++){                             //Die Musteroptionen werden einzeln durchgegangen.
            String aktuellesMerkmal = musterOptionen[k];                            //Das aktuelle Merkmal wird zwischengespeichert.

            for(int j = 0; j < schmetterlinge.length; j++){                         //Es wird durch die Menge an Schmetterlingen iteriert.
                if(schmetterlinge[j].getMerkmale()[0].equals(aktuellesMerkmal)){
                    zaehler[k]++;                                                   //Falls der Schmetterling das Merkmal aufweist, wird der Schmetterling gezählt.
                }
            }
            System.out.print(musterOptionen[k] + ": " + (double)(zaehler[k] * 10000 / schmetterlinge.length) / 100 + "%   ");   //Das Ergebnis wird prozentual ausgegeben.
        }
        System.out.println();                                                       //Eine neue Zeile der Ausgabe wird aangefangen.
        zaehler = new int[4];
        for(int k = 0; k < farbOptionen.length; k++){                               //Die Farboptionen werden einzeln durchgegangen.
            String aktuellesMerkmal = farbOptionen[k];                              //Das aktuelle Merkmal wird zwischengespeichert.

            for(int j = 0; j < schmetterlinge.length; j++){                         //Es wird durch die Menge an Schmetterlingen iteriert.
                if(schmetterlinge[j].getMerkmale()[1].equals(aktuellesMerkmal)){
                    zaehler[k]++;                                                   //Falls der Schmetterling das Merkmal aufweist, wird der Schmetterling gezählt.
                }
            }
            System.out.print(farbOptionen[k] + ": " + (double)(zaehler[k] * 10000 / schmetterlinge.length) / 100 + "%   ");     //Das Ergebnis wird prozentual ausgegeben.
        }
        System.out.println();                                                       //Eine neue Zeile der Ausgabe wird aangefangen.
        zaehler = new int[2];
        for(int k = 0; k < fuehlerOptionen.length; k++){                            //Die Fühleroptionen werden einzeln durchgegangen.
            String aktuellesMerkmal = fuehlerOptionen[k];                           //Das aktuelle Merkmal wird zwischengespeichert.

            for(int j = 0; j < schmetterlinge.length; j++){                         //Es wird durch die Menge an Schmetterlingen iteriert.
                if(schmetterlinge[j].getMerkmale()[2].equals(aktuellesMerkmal)){
                    zaehler[k]++;                                                   //Falls der Schmetterling das Merkmal aufweist, wird der Schmetterling gezählt.
                }
            }
            System.out.print(fuehlerOptionen[k] + ": " + (double)(zaehler[k] * 10000 / schmetterlinge.length) / 100 + "%   ");  //Das Ergebnis wird prozentual ausgegeben.
        }
    }

    /**
     * Die Methode filtern nimmt eine Menge an Schmetterlingen entgegen und ein Merkmal, nach welchem gefiltert wird.
     * Zuerst wird gezaehlt, wieviele Schmetterlinge dieses Merkmal aufweisen, um dann ein Array derselben Groesse zu erstellen.
     * Danach wird fuer jeden  Schmetterling, anhand des Merkmals entschieden, ob er in das neue Array gespeichert wird.
     * Zuletzt wird die Teilmenge der Schmetterlinge ausgegeben die das Merkmal aufweisen, sowie die Anzahl derselben.
     * @param merkmal
     * @param schmetterlinge
     * @return auswahl Die Auswahl an gefilterten Schmetterlingen aus der Brut
     */
    public Schmetterling[] filtern(String merkmal, Schmetterling[] schmetterlinge){
        int zaehler = 0;
        for(int x = 0; x < schmetterlinge.length; x++){
            for(int y = 0; y < 3; y++){
                if(schmetterlinge[x].getMerkmale()[y].toUpperCase().equals(merkmal.toUpperCase())){
                    zaehler++;
                }
            }
        }
        Schmetterling[] auswahl = new Schmetterling[zaehler];
        int filterZaehler = 0;
        for(int x = 0; x < schmetterlinge.length; x++){
            for(int y = 0; y < 3; y++){
                if(schmetterlinge[x].getMerkmale()[y].toUpperCase().equals(merkmal.toUpperCase())){
                    auswahl[filterZaehler] = schmetterlinge[x];
                    filterZaehler++;
                }
            }
        }
        printSchmetterlinge(auswahl);
        System.out.println("In der Brut gibt es " + zaehler + " Schmetterlinge mit dem Merkmal: '" + merkmal + "'");
        return auswahl;
    }

    /**
     * Diese Methode nimmt zwei Strings entgegen und gibt zufaellig eines der beiden zurück.
     * @param option1 Die erste Option
     * @param option2 Die zweite Option
     * @return merkmal Das zufaellige Merkmal
     */
    private String randomMerkmal(String option1, String option2){
        String resultat;
        String[] optionen = new String[]{option1, option2};
        int index = (int) (Math.random() * (double) (2));
        resultat = optionen[index];
        return resultat;
    }

    /**
     * Diese Methode gibt eine Zufallszahl zwischen 1000 und 10000 aus.
     * @return zahluni
     */
    private int randomZahl(){
        int zahl = (int) (Math.random() * (double) (10000 - 1000 + 1) ) + 1000;
        return zahl;
    }

    /**
     * Getter-Methode fuer Schmetterling[] schmetterlinge
     * @return schmetterlinge
     */
    public Schmetterling[] getSchmetterlinge() {

        return schmetterlinge;
    }

    /**
     * Diese Methode nimmt ein Array an Schmetterlingen entgegen. Und gibt jeden Schmetterling zeilenweise auf das Terminal aus.
     * @param schmetterlinge eine Menge an Schmetterlingen als Array
     */
    public void printSchmetterlinge(Schmetterling[] schmetterlinge){
        for(int k = 0; k < schmetterlinge.length; k++){
            System.out.println(schmetterlinge[k].getMerkmale()[0] + " " + schmetterlinge[k].getMerkmale()[1] + " " + schmetterlinge[k].getMerkmale()[2]);
        }
    }
}
