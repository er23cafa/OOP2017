package uni.leipzig.oop2017.aufgabe13;

import java.util.HashSet;
import java.util.Scanner;

/**
 * Created by Emil Rode und Nora Brummel on 20.04.17.
 */
public class Main {


    public static void main(String[]args){

        /**
         * String[] merkmaleEltern: eingegebene Merkmale von Vater und Mutter
         * Scanner eingabeScanner: Scanner, der die Eingaben des Nutzers regelt
         */
        String[] merkmaleEltern = new String[6];
        Scanner eingabeScanner = new Scanner(System.in);
        /**
         * * HashSet<String> musterungSet: HashSet, das mögliche Musterungen enthält
         */
        HashSet<String> musterungSet = new HashSet<>();
        musterungSet.add("uni");
        musterungSet.add("gepunktet");
        musterungSet.add("gestreift");

        /**
         * HashSet<String> fluegelfarbeSet: HashSet, das mögliche Flügelfarben enthält
         */
        HashSet<String> fluegelfarbeSet = new HashSet<>();
        fluegelfarbeSet.add("rot");
        fluegelfarbeSet.add("gelb");
        fluegelfarbeSet.add("grün");
        fluegelfarbeSet.add("blau");

        /**
         * HashSet<String> fuehlerformSet: HashSet, das mögliche Fühlerformen enthält
         */
        HashSet<String> fuehlerformSet = new HashSet<>();
        fuehlerformSet.add("gerade");
        fuehlerformSet.add("gekrümmt");

        System.out.println(fluegelfarbeSet.contains("hallo"));

        /*
            Initialisierungs- Schleife: läuft, solange Merkmale von Mutter und Vater bestimmt werden
         */
        initializingLoop: while(true) {
            try {
                System.out.println("\nBitte geben Sie die gewünschten Merkmale der Mutter in folgender Form an: " +
                        "\nMusterung Flügelfarbe Fühlerform\n" +
                        "\nZur Auswahl stehen: \n" +
                        "\nMusterung: uni / gepunktet / gestreift" +
                        "\nFlügelfarbe: rot / gelb / grün / blau" +
                        "\nFühlerform: gerade / gekrümmt\n" +
                        "\nIhre Eingabe: ");
                String eingabeMama = eingabeScanner.nextLine(); //Nutzer gibt gewünschte Merkmale der Mutter ein
                String[] eingabeMamaSplit = eingabeMama.split(" "); //Eingabe wird in drei Teile unterteilt
                merkmaleEltern[0] = eingabeMamaSplit[0]; //erste Stelle von merkmaleEltern[] entspricht Musterung der Mutter
                merkmaleEltern[1] = eingabeMamaSplit[1]; //zweite Stelle von merkmaleEltern[] entspricht Flügelfarbe der Mutter
                merkmaleEltern[2] = eingabeMamaSplit[2]; //dritte Stelle von mermaleEltern[] entspricht Fühlerform der Mutter

                System.out.println("\nBitte geben Sie die gewünschten Merkmale des Vaters in folgender Form an: " +
                        "\nMusterung Flügelfarbe Fühlerform\n" +
                        "\nZur Auswahl stehen: \n" +
                        "\nMusterung: uni / gepunktet / gestreift" +
                        "\nFlügelfarbe: rot / gelb / grün / blau" +
                        "\nFühlerform: gerade / gekrümmt\n" +
                        "\nIhre Eingabe: ");
                String eingabePapa = eingabeScanner.nextLine();
                String[] eingabePapaSplit = eingabePapa.split(" ");
                merkmaleEltern[3] = eingabePapaSplit[0]; //vierte Stelle von merkmaleEltern[] entspricht Musterung des Vaters
                merkmaleEltern[4] = eingabePapaSplit[1]; //fünfte Stelle von merkmaleEltern[] entspricht Flügelfarbe des Vaters
                merkmaleEltern[5] = eingabePapaSplit[2]; //sechste Stelle von merkmaleEltern[] entspricht Fühlerform des Vaters


                if (!(musterungSet.contains(merkmaleEltern[0]) && musterungSet.contains(merkmaleEltern[3]))) {
                    System.out.println("Sie haben einen Fehler bei 'Musterung' gemacht. Bitte überprüfen Sie Ihre Eingabe bezüglich Rechtschreibung und Leerzeichen.\n");
                    continue initializingLoop;
                }
                /*
                    wird ausgegeben, wenn die Eingabe der Musterung der Mutter/ des Vaters nicht einer der vorgegebenen
                    Möglcihkeiten entspricht
                 */
                if (!(fluegelfarbeSet.contains(merkmaleEltern[1]) && fluegelfarbeSet.contains(merkmaleEltern[4]))) {
                    System.out.println("Sie haben einen Fehler bei 'Flügelfarbe' gemacht. Bitte überprüfen Sie Ihre Eingabe bezüglich Rechtschreibung und Leerzeichen.\n");
                    continue initializingLoop;
                }
                /*
                    wird ausgegeben, wenn die Eingabe der Flügelfarbe der Mutter/ des Vaters nicht einer der vorgegebenen
                    Möglichkeiten entspricht
                 */
                if (!(fuehlerformSet.contains(merkmaleEltern[2]) && fuehlerformSet.contains(merkmaleEltern[5]))){
                    System.out.println("Sie haben einen Fehler bei 'Fühlerform' gemacht. Bitte überprüfen Sie Ihre Eingabe bezüglich Rechtschreibung und Leerzeichen.\n");
                    continue initializingLoop;
                }
                /*
                    wird ausgegeben, wenn die Eingabe der Fühlerform der Mutter/ des Vaters nicht einer der vorgegebenen
                    Möglichkeiten entspricht
                 */
            } catch (ArrayIndexOutOfBoundsException e) {
                System.out.println("Sie haben einen Fehler gemacht. Bitte überprüfen Sie Ihre Eingabe bezüglich Rechtschreibung und Leerzeichen.\n");
                continue initializingLoop;
            }
            /*
                fängt eine mögliche ArrayIndexOutOfBoundsException e
             */

            Brut brut = new Brut(merkmaleEltern);
            /*
                Command - Schleife: läuft solange der Nutzer Aktionen ausführen möchte
                beinhaltet switch- Statement, das Aktionen ausführt, je nachdem, was der Nutzer wünscht
             */
            commandLoop: while (true) {
                System.out.println();
                System.out.println("Was möchten Sie tun?" +
                        "\n * Brut ausgeben" + "\n * Nach Merkmal filtern" + "\n * Brut auswerten" + "\nIhre Eingabe: ");
                switch (eingabeScanner.nextLine().toUpperCase()) {
                    case "BRUT AUSGEBEN":
                        brut.printSchmetterlinge(brut.getSchmetterlinge());
                        break;
                    case "NACH MERKMAL FILTERN":
                        System.out.println("Bitte geben Sie ein Merkmal ein. Die Schmetterlinge der Brut, die dieses Merkmal enthalten, werden für Sie aussortiert." +
                                "\nIhre Eingabe: ");
                        Schmetterling[] basis = brut.getSchmetterlinge();
                        /*
                            Filter - Schleife: läuft, während Brut nach Merkmal gefiltert wird
                         */
                        filterLoop: while(true){
                            String eingabeMerkmal = eingabeScanner.nextLine();
                            if(musterungSet.contains(eingabeMerkmal) || fluegelfarbeSet.contains(eingabeMerkmal) || fuehlerformSet.contains(eingabeMerkmal)) {
                                basis = brut.filtern(eingabeMerkmal, basis);
                                /*repeatFilterLoop:
                                while (true) {
                                    System.out.println("Möchten Sie die gefilterte Brut noch einmal nach einem anderen Merkmal filtern? (JA/NEIN)");
                                    switch (eingabeScanner.nextLine().toUpperCase()) {
                                        case "JA":
                                            System.out.println("Bitte geben Sie ein Merkmal ein: ");
                                            String repeatEingabeMerkmal = eingabeScanner.nextLine();
                                            if (musterungSet.contains(repeatEingabeMerkmal) || fluegelfarbeSet.contains(repeatEingabeMerkmal) || fuehlerformSet.contains(repeatEingabeMerkmal)) {
                                                basis = brut.filtern(eingabeMerkmal, basis);
                                            } else{
                                                System.out.println("Dies ist kein gültiges Merkmal.");
                                            }
                                            continue repeatFilterLoop;
                                        case "NEIN": break filterLoop;
                                        default:
                                            System.out.print("Du honk du sollst ja oder nein schreiben!");
                                            continue repeatFilterLoop;
                                    }
                                }
                            }
                            else{
                                        System.out.println("Dies ist kein gültiges Merkmal.");
                                        continue filterLoop;
                                    */
                                break filterLoop;
                            }
                        }
                        break;
                    case "BRUT AUSWERTEN":
                        brut.auswerten();
                        break;
                    case "CODE NORBERT"  :
                        System.out.println("   *   *   ***   ****    ****   ****  ****  *****     *****");
                        System.out.println("   **  *  *   *  *   *   *   *  *     *   *   *        * * ");
                        System.out.println("   * * *  *   *  ****    ****   ***   ****    *         *  ");
                        System.out.println("   *  **  *   *  *   *   *   *  *     *   *   *            ");
                        System.out.println("   *   *   ***   *    *  ****   ****  *    *  *         *  \n");
                        System.out.println("Diiiiies hackskillz JÜÜÜNGE!");
                        System.exit(0);
                    default:
                        System.out.println("Bitte geben sie einen gültigen Befehl ein.");
                        continue commandLoop;
                }
                System.out.println("\nMöchten Sie noch etwas tun? (JA/NEIN)");
                /*
                    checkForExit: fragt, ob Nutzer beenden oder weitermachen möchte und führt dementsprechend Aktion aus
                 */
                checkForExit: while( true ) {
                    switch (eingabeScanner.nextLine().toUpperCase()) {
                        case "JA":
                            break checkForExit;
                        case "NEIN":
                            System.out.println("\nBis zum nächsten mal, viel Spaß mit den Schmetterlingen! <3");
                            System.exit(0);
                            break;
                        default:
                            System.out.println("Was können Sie eigentlich? Es hieß doch 'JA oder NEIN'");
                            System.out.println("Versuchen Sie es nochmal:");
                    }
                }
            }
        }
    }
}


